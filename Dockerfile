FROM registry.cern.ch/docker.io/library/python:3-slim
ARG DEBIAN_FRONTEND=noninteractive
RUN pip install --no-cache-dir twine
